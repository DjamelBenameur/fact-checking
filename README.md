Le but de ce projet consiste à mettre en oeuvre et évaluer des méthodes de classification de
documents d’opinions

**Le corpus :**


Un jeu de données textuelles est mis à disposition sur Moodle. Il s'agit d'un corpus d'à peu près
8000 documents contenant des avis d'internautes sur des films. A chaque document est associé sa
polarité selon l'avis (+1 : positif, -1 : négatif). Le fichier des documents est formaté dans un tableau
cvs (un avis par ligne), un autre fichier csv contient les polarités d'avis par document (- 1/+1). Une
correspondance directe existe entre les numéros des lignes des documents et des polarités.

**Objectifs :**

L’objectif du projet est de classer le mieux possible les avis. Pour cela vous pouvez utiliser
différentes versions du corpus : textes bruts avec ou sans suppression de stop-words, textes
lémmatisés, stématisés, utilisation d’un outil de part-of-speech-tagging (librairie NLTK, TreeTagger
– http://www.cis.unimuenchen.de/~schmid/tools/TreeTagger/), utilisation de n-grams, etc.
Vous devrez rechercher des ressources externes pour traiter de l’ironie et aider à la classification. Il
y a beaucoup d’ironie dans les avis. 
Vous devrez utiliser différents classifieurs pour voir ceux qui donnent les meilleurs résultats, tester
les éventuels hyperparamètres pour au final fournir un modèle qui soit apte à être utilisé avec des
données de même type. 

**Code :**

https://colab.research.google.com/drive/19TJDxD6cwRaWhPd4HeE1CBCucq3mc-UU?usp=sharing#scrollTo=mkIxc7-LFCCt